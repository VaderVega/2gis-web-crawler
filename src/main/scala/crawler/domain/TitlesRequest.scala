package crawler.domain

import cats.effect.kernel.Concurrent
import crawler.config.Types.URL
import derevo.cats.show
import derevo.circe.magnolia.decoder
import derevo.derive
import io.circe.refined._
import eu.timepit.refined.cats._
import org.http4s.EntityDecoder
import org.http4s.circe.jsonOf


@derive(decoder, show)
case class TitlesRequest(urls: List[URL])

object TitlesRequest {

  implicit def titlesRequestEntityDecoder[F[_]: Concurrent]: EntityDecoder[F, TitlesRequest] =
    jsonOf[F, TitlesRequest]
}
