package crawler.domain

import cats.effect.Concurrent
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder
import org.http4s.EntityEncoder
import org.http4s.circe.jsonEncoderOf

case class TitlesResponse(processedUrls: Map[String, String])

object TitlesResponse {
  implicit val titleResponseEncoder: Encoder[TitlesResponse] =
    deriveEncoder[TitlesResponse]

  implicit def titleResponseEntityEncoder[F[_] : Concurrent]: EntityEncoder[F, TitlesResponse] =
    jsonEncoderOf[F, TitlesResponse]
}
