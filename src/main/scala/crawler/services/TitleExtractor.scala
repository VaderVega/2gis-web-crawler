package crawler.services

import cats.Parallel
import cats.effect.Sync
import cats.implicits._
import crawler.config.Types.{URL, UrlTitlePair}
import crawler.utils.CommonOps.ListOps
import dev.profunktor.redis4cats.RedisCommands
import org.jsoup.Jsoup

import scala.concurrent.duration._

class TitleExtractor[F[_] : Sync] private(redis: RedisCommands[F, String, String]) extends TitleExtractorAlg[F] {

  def extractTitleTagForSite(urls: List[URL], batchSize: Int)(implicit P: Parallel[F]): F[List[UrlTitlePair]] = {
    urls
      .distinct
      .batchTraverse[F, UrlTitlePair](batchSize)(l => l.parTraverse(extractTitle))
  }

  private[services] def extractTitle(url: URL): F[(String, String)] = for {
    maybeTitle <- getFromCache(url.value)
    title <- maybeTitle match {
      case Some(t) => t.pure[F]
      case None => getTitleFromNetwork(url.value)
    }
  } yield url.value -> title

  private[services] def getFromCache(url: String): F[Option[String]] =
    redis.get(url)

  private[services] def getTitleFromNetwork(url: String): F[String] = {
    Sync[F].blocking {
      Jsoup
        .connect(url)
        .get()
        .title()
    }.flatMap { title =>
      addToCache(url, title) >> title.pure[F]
    }
  }

  private[services] def addToCache(url: String, title: String): F[Unit] =
    redis.set(url, title) *> redis.expire(url, 1.hour).void
}


object TitleExtractor {
  def make[F[_] : Sync](redis: RedisCommands[F, String, String]) =
    new TitleExtractor[F](redis)
}
