package crawler.services

import crawler.domain.healthcheck._
import cats.effect._
import cats.effect.implicits._
import cats.syntax.all._
import dev.profunktor.redis4cats.RedisCommands

import scala.concurrent.duration._

trait HealthCheck[F[_]] {
  def status: F[AppStatus]
}

object HealthCheck {
  def make[F[_] : Temporal](redis: RedisCommands[F, String, String]): HealthCheck[F] =
    new HealthCheck[F] {
      val redisHealth: F[RedisStatus] =
        redis.ping
          .map(_.nonEmpty)
          .timeout(1.second)
          .map(Status._Bool.reverseGet)
          .orElse(Status.Unreachable.pure[F].widen)
          .map(RedisStatus.apply)

      val status: F[AppStatus] = redisHealth.map(AppStatus.apply)
    }
}
