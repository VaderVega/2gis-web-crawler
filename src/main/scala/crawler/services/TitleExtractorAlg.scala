package crawler.services

import cats.Parallel
import crawler.config.Types.{URL, UrlTitlePair}

trait TitleExtractorAlg[F[_]] {
  def extractTitleTagForSite(urls: List[URL], batchSize: Int)(implicit P: Parallel[F]): F[List[UrlTitlePair]]
}
