package crawler.modules

import cats.Parallel
import cats.effect.Async
import cats.syntax.all._
import crawler.http.routes.{HealthCheckRoutes, TitleRoutes}
import org.http4s.implicits._
import org.http4s.server.middleware._
import org.http4s.{HttpApp, HttpRoutes}

import scala.concurrent.duration._

sealed abstract class HttpApi[F[_] : Async : Parallel] private(
                                                                services: Services[F],
                                                                batchSize: Int
                                                              ) {
  private val titleRoutes = TitleRoutes[F](services.titleExtractor, batchSize).routes

  private val healthcheckRoutes = HealthCheckRoutes[F](services.healthCheck).routes

  private val middleware: HttpRoutes[F] => HttpRoutes[F] = {
    { http: HttpRoutes[F] =>
      AutoSlash(http)
    } andThen { http: HttpRoutes[F] =>
      Timeout(60.seconds)(http)
    }
  }

  private val loggers: HttpApp[F] => HttpApp[F] = {
    {
      http: HttpApp[F] =>
        RequestLogger.httpApp(true, true)(http)
    } andThen { http: HttpApp[F] =>
      ResponseLogger.httpApp(true, true)(http)
    }
  }

  private val routes: HttpRoutes[F] = titleRoutes <+> healthcheckRoutes

  val httpApp: HttpApp[F] = loggers(middleware(routes).orNotFound)
}

object HttpApi {
  def make[F[_] : Async : Parallel](
                        services: Services[F],
                        batchSize: Int
                        ): HttpApi[F] =
    new HttpApi[F](services, batchSize) {}
}
