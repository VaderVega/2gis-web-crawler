package crawler.modules

import cats.effect._
import crawler.services.{HealthCheck, TitleExtractor, TitleExtractorAlg}
import dev.profunktor.redis4cats.RedisCommands

sealed abstract class Services[F[_]] private (val titleExtractor: TitleExtractorAlg[F],
                                              val healthCheck: HealthCheck[F])

object Services {
  def make[F[_] : Temporal : Sync](redis: RedisCommands[F, String, String]): Services[F] = {
    val titleExtractor = TitleExtractor.make[F](redis)
    val healthCheck = HealthCheck.make[F](redis)
    new Services[F](titleExtractor, healthCheck) {}
  }
}
