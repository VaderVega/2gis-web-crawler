package crawler

import cats.effect._
import cats.effect.std.Supervisor
import crawler.config.Config
import crawler.modules.{HttpApi, Services}
import crawler.resources.{AppResources, MkHttpServer}
import dev.profunktor.redis4cats.log4cats._
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

object Main extends IOApp.Simple {

  implicit val logger = Slf4jLogger.getLogger[IO]

  override def run: IO[Unit] =
    Config.load[IO].flatMap { cfg =>
      Logger[IO].info(s"Loaded config $cfg") >>
        Supervisor[IO].use { implicit sp =>
          AppResources
            .make[IO](cfg)
            .evalMap { res =>
              IO {
                res.redis.enableAutoFlush
                val services = Services.make[IO](res.redis)
                val httpApi = HttpApi.make[IO](services, cfg.batchSize)
                cfg.httpServerConfig -> httpApi.httpApp
              }
            }.flatMap {
            case (cfg, httpApp) => MkHttpServer[IO].newEmber(cfg, httpApp)
          }.useForever
        }
    }

}
