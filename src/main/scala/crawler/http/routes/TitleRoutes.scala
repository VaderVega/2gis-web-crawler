package crawler.http.routes

import cats.effect.Concurrent
import cats.implicits._
import cats.{Monad, MonadThrow, Parallel}
import crawler.domain.{TitlesRequest, TitlesResponse}
import crawler.services.TitleExtractorAlg
import org.http4s._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.dsl._


final case class TitleRoutes[F[_] : Concurrent : Monad : MonadThrow : Parallel](titleExtractor: TitleExtractorAlg[F],
                                                                                batchSize: Int)
                                                                    extends Http4sDsl[F] {

  val routes: HttpRoutes[F] = HttpRoutes.of[F] {
    case req @ POST -> Root / "titles" =>
      req.as[TitlesRequest].flatMap { r =>
        val titles = for {
          titlesPairs <- titleExtractor.extractTitleTagForSite(r.urls, batchSize)
        } yield TitlesResponse(titlesPairs.toMap)
        Ok(titles)
      }
  }
}
