package crawler.resources

import cats.effect.std.Console
import cats.effect.{Concurrent, Resource}
import cats.syntax.all._
import crawler.config.Types._
import eu.timepit.refined.auto._
import dev.profunktor.redis4cats.effect.MkRedis
import dev.profunktor.redis4cats.{Redis, RedisCommands}
import org.typelevel.log4cats.Logger

sealed abstract class AppResources[F[_]](val redis: RedisCommands[F, String, String])

object AppResources {
  def make[F[_] : Concurrent : Console : Logger : MkRedis](cfg: AppConfig): Resource[F, AppResources[F]] = {
    def checkRedisConnection(redis: RedisCommands[F, String, String]): F[Unit] =
      redis.info.flatMap {
        _.get("redis_version").traverse_ { v =>
          Logger[F].info(s"Connected to Redis $v")
        }
      }

    def mkRedisResource(redisConfig: RedisConfig): Resource[F, RedisCommands[F, String, String]] =
      Redis[F].utf8(redisConfig.uri.value).evalTap(checkRedisConnection)

    mkRedisResource(cfg.redisConfig).map(new AppResources[F](_) {})
  }
}
