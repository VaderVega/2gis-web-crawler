package crawler.resources

import cats.effect.{Async, Resource}
import crawler.config.Types.HttpServerConfig
import org.http4s.HttpApp
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.Server
import org.typelevel.log4cats.Logger
import org.http4s.server.defaults.Banner

trait MkHttpServer[F[_]] {
  def newEmber(cfg: HttpServerConfig, httpApp: HttpApp[F]): Resource[F, Server]
}

object MkHttpServer {
  def apply[F[_] : MkHttpServer]: MkHttpServer[F] = implicitly

  private def showEmberBanner[F[_] : Logger](server: Server): F[Unit] =
    Logger[F].info(s"\n${Banner.mkString("\n")}\nHTTP Server started ad ${server.address}")

  implicit def forAsyncLogger[F[_] : Async : Logger]: MkHttpServer[F] =
    (cfg: HttpServerConfig, httpApp: HttpApp[F]) => EmberServerBuilder
      .default[F]
      .withHost(cfg.host)
      .withPort(cfg.port)
      .withHttpApp(httpApp)
      .build
      .evalTap(showEmberBanner[F])
}
