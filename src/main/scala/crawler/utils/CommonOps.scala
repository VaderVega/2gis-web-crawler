package crawler.utils

import cats.Monad
import cats.implicits._

object CommonOps {

  implicit class ListOps[A](val list: List[A]) extends AnyVal {
    def batchTraverse[F[_] : Monad, B](batchSize: Int)(f: List[A] => F[List[B]]): F[List[B]] = {
      val fs = list.grouped(batchSize)
        .toList
        .map(f)
      def add(bs1: F[List[B]], bs2: F[List[B]]): F[List[B]] = for {
        bs1 <- bs1
        bs2 <- bs2
      } yield bs1 ++ bs2
      fs.foldLeft(List[B]().pure[F])(add)
    }
  }

}
