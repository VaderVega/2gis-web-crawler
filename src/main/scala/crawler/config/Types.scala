package crawler.config

import com.comcast.ip4s.{Host, Port}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.string.Url
import eu.timepit.refined.types.string.NonEmptyString
import io.estatico.newtype.macros.newtype

object Types {
  case class HttpServerConfig(host: Host, port: Port)

  @newtype case class RedisURI(value: NonEmptyString)
  @newtype case class RedisConfig(uri: RedisURI)

  case class AppConfig(httpServerConfig: HttpServerConfig,
                       redisConfig: RedisConfig,
                       batchSize: Int)

  type URL = String Refined Url
  type UrlTitlePair = (String, String)
}
