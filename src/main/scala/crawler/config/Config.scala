package crawler.config

import cats.effect.Async
import cats.syntax.all._
import ciris._
import ciris.refined._
import com.comcast.ip4s.Literals.port
import com.comcast.ip4s._
import crawler.config.AppEnvironment._
import crawler.config.Types._
import eu.timepit.refined.auto._
import eu.timepit.refined.types.string.NonEmptyString

object Config {

  def load[F[_]: Async]: F[AppConfig] =
    env("APP_ENV")
      .as[AppEnvironment]
      .flatMap {
        case Test =>
          default[F](
            RedisURI("redis://localhost")
          )
        case Prod =>
          default[F](
            RedisURI("redis://localhost") // change to prod if you have it
          )
      }.load[F]

  private def default[F[_]](redisUri: RedisURI): ConfigValue[F, AppConfig] = {
    (
      env("HOST").as[NonEmptyString],
      env("PORT").as[Int],
      env("BATCH_SIZE").as[Int]
    ).parMapN { (h, p, batchSize) =>
      val host: Host = Host.fromString(h).getOrElse(host"0.0.0.0")
      val port: Port = Port.fromInt(p).getOrElse(port"8080")
      AppConfig(
        HttpServerConfig(
          host = host,
          port = port
        ),
        RedisConfig(redisUri),
        batchSize
      )
    }
  }

}
