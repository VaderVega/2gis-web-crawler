package crawler

import eu.timepit.refined.api.Refined
import eu.timepit.refined.scalacheck.arbitraryRefType
import eu.timepit.refined.string.Url
import org.scalacheck.{Arbitrary, Gen}


object Generators {
  private val protocols = Seq("http://", "https://")
  private val domains = Seq(
    "google.com",
    "youtube.com",
    "facebook.com",
    "vk.com",
    "instagram.com",
    "mail.ru",
    "yandex.ru"
  )

  def urlGen: Gen[String Refined Url] = {
    val urlAuxGen: Arbitrary[String Refined Url] = arbitraryRefType(urlStringGen)
    urlAuxGen.arbitrary
  }

  def urlStringGen: Gen[String] = for {
    protocol <- Gen.oneOf(protocols)
    domain <- Gen.oneOf(domains)
  } yield protocol + domain

}
