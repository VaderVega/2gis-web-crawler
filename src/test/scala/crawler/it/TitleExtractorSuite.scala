package crawler.it

import cats.effect._
import cats.implicits._
import crawler.Generators.urlGen
import crawler.services.TitleExtractor
import crawler.suite.ResourceSuite
import dev.profunktor.redis4cats.log4cats._
import dev.profunktor.redis4cats.{Redis, RedisCommands}
import eu.timepit.refined.api.RefType.refinedRefType
import eu.timepit.refined.cats._
import org.typelevel.log4cats.noop.NoOpLogger

object TitleExtractorSuite extends ResourceSuite {

  implicit val logger = NoOpLogger[IO]

  type Res = RedisCommands[IO, String, String]

  private val expectedTitles = List(
    "Google",
    "YouTube",
    "Facebook",
    "Добро пожаловать | ВКонтакте",
    "Instagram",
    "Mail.ru: почта, поиск в интернете, новости, игры",
    "Яндекс"
  )

  override def sharedResource: Resource[IO, Res] =
    Redis[IO]
      .utf8("redis://localhost")
      .beforeAll(_.flushAll)

  test("Titles are extracting correctly") { redis =>
    val gen = urlGen
    val titleExtractor = TitleExtractor.make[IO](redis)

    forall(gen) { url =>
      for {
        titles <- titleExtractor.extractTitleTagForSite(List(url), 10)
        titlesMap = titles.toMap
      } yield expect(expectedTitles.contains(titlesMap(url.value)))
    }
  }
}
