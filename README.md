web-crawler
===========

This is a simple web crawler, based on Cats Effect with Redis as a cache, that extracts the content
of title tag from a web page.

For running Redis locally use the command `docker-compose up` from the root project directory.

## Endpoints
In this small microservice there are two endpoints. The first for extracting title from web pages,
and the second for checking redis state.

**POST /titles**

Example of the request body for POST request:
```
{
    "urls": [
        "https://vk.com",
        "http://google.com",
        "https://youtube.com"
    ]
}
```

Response from this endpoint looks like this:
```
{
    "processedUrls": {
        "https://vk.com/": "Добро пожаловать | ВКонтакте",
        "http://google.com/": "Google",
        "http://youtube.com/": "YouTube"
    }
}
```

**GET /healthcheck**

This endpoint returns the Redis status. Response from this endpoint look like this:
```
{
    "redis": {
        "status": "Okay"
    }
}
```
